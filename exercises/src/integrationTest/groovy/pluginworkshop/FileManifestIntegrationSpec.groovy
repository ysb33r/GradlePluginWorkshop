// tag::initial-setup[]
package pluginworkshop

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class FileManifestIntegrationSpec extends Specification { // <1>

    @Rule final TemporaryFolder testProjectDir = new TemporaryFolder() // <2>
// end::initial-setup[]

    // tag::test-example[]
    def 'FileManifest must list each file with absolute path and size'() {

        setup:
        new File(testProjectDir.root,'build.gradle').text='''
plugins {
  id 'com.example.pluginworkshop'
}

task createFiles {
    doLast {
        mkdir buildDir
        (1..4).each {
            new File(buildDir,"${it}.txt").text='a'.multiply(it)
        }
    }
}

fileManifest{
    
  (1..4).collect {
    manifestFiles new File(buildDir,"${it}.txt")
  }
   
  dependsOn createFiles
}

''' // <1>

        when:
        def testSetup = GradleRunner.create() // <2>
            .withProjectDir(testProjectDir.root)  // <3>
            .withArguments('fileManifest') // <4>
            .forwardOutput() // <5>

        // end::test-example[]
        // tag::pluginclasspath[]
        testSetup.withPluginClasspath( [new File('./build/libs/gradle-workshop-plugin-1.0-SNAPSHOT.jar')] )
        // tag::test-example[]

        def result = testSetup.build()
        // end::pluginclasspath[]

        then:
        result.task(":fileManifest").outcome == TaskOutcome.SUCCESS // <6>
        new File(testProjectDir.root,'build/workshop-manifest.txt').exists() // <7>
    }
    // end::test-example[]

    // tag::static-properties[]
    static final File PLUGIN_CLASSPATH = new File ( System.getProperty('PLUGIN_METADATA_FILE') ?: './build/integrationTest/manifest/plugin-classpath.txt')
    // end::static-properties[]

    def 'FileManifest must list each file with absolute path and size (try 2)'() {

        setup:
        new File(testProjectDir.root,'build.gradle').text='''
plugins {
  id 'com.example.pluginworkshop'
}

task createFiles {
    doLast {
        mkdir buildDir
        (1..4).each {
            new File(buildDir,"${it}.txt").text='a'.multiply(it)
        }
    }
}

fileManifest{
    
  (1..4).collect {
    manifestFiles new File(buildDir,"${it}.txt")
  }
   
  dependsOn createFiles
}

'''
        // tag::with-plugin-classpath-file[]
        when:
        def testSetup = GradleRunner.create()
            .withProjectDir(testProjectDir.root)
            .withArguments('fileManifest')
            .forwardOutput()

        testSetup.withPluginClasspath( PLUGIN_CLASSPATH.readLines().collect {
            new File(it)
        } )

        def result = testSetup.build()
        // end::with-plugin-classpath-file[]

        then:
        result.task(":fileManifest").outcome == TaskOutcome.SUCCESS // <6>
        new File(testProjectDir.root,'build/workshop-manifest.txt').exists() // <7>
    }

// tag::initial-setup[]
}
// end::initial-setup[]
