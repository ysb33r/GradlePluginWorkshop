// tag::first-layout[]
package pluginworkshop

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.tasks.bundling.Zip

@CompileStatic // <1>
class WorkshopPlugin implements Plugin<Project> { // <2>

    // tag::with-simple-extension[]
    @Override
    void apply(Project project) {
        // end::first-layout[]
        project.extensions.extraProperties.set('workshop','Hello, Gr8Conf 2018!') // <1>
    // end::with-simple-extension[]

        // tag::file-manifest[]
        project.tasks.create('fileManifest',FileManifest) // <1>
        // end::file-manifest[]

        // tag::hashtags[]
        project.extensions.create('hashtags',HashtagExtension,project) // <1>
        // end::hashtags[]

        // tag::look-for-zip-tasks[]
        project.tasks.withType(Zip) { Zip task ->  // <1>
            WorkshopPlugin.addManifestToZip(project,task)
        }
        // end::look-for-zip-tasks[]

        // tag::add-trigger-for-zip[]
        project.tasks.whenTaskAdded { Task task ->
            if(task instanceof Zip) { // <1>
                WorkshopPlugin.addManifestToZip(task.project,(Zip)task)
            }
        }
        // end::add-trigger-for-zip[]
        // tag::first-layout[]
    }
    // end::first-layout[]
    // end::with-simple-extension[]

    // tag::modify-zip-tasks[]
    static void addManifestToZip( Project project, Zip task ) {
        task.dependsOn 'fileManifest' // <1>
        task.from project.tasks.getByName('fileManifest') // <2>
    }
    // end::modify-zip-tasks[]
// tag::first-layout[]
}
// end::first-layout[]
