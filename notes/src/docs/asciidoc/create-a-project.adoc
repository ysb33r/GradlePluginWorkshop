= Writing Gradle Plugins in Groovy : Project Basics
Schalk W. Cronjé <ysb33r@gmail.com>

== Create a project

[listing,subs="attributes"]
----
mkdir MyPlugin
cd MyPlugin
gradle wrapper --gradle-version={gradle-version}
----

== Name the project

Create a `settings.gradle` file.

.settings.gradle
[source,groovy]
----
include::{exercisesdir}/settings.gradle[tags=name]
----

== Layout the basics

Create a `build.gradle` file.

.build.gradle
[source,groovy]
----
include::{exercisesdir}/build.gradle[tags=building-with-groovy]
----
<1> `localGroovy` always references the version of Groovy that ships with the curretn version of Gradle.
<2> `gradleApi` will place the appropriate Gradle API on the classpath.

== Setup for unit testing

Edit the `build.gradle` file and add the following.

.build.gradle
[source,groovy]
----
include::{exercisesdir}/build.gradle[tags=testing-with-spock]
----
<1> Calculate the major-minor version of Groovy currently used by Gradle.
<2> This set the correct dependency for the Spock Framework to track the Groovy version shipped with Gradle.
<3> Prevent Spock Framework transitive dependencies from adding another version of Groovy.

== Create source directories

[listing]
----
mkdir -p src/main/groovy // <1>
mkdir -p src/main/resources/META-INF/gradle-plugins // <2>
mkdir -p src/test/groovy // <3>
----
<1> For plugin source code.
<2> This is where service metadata for plugin will be stored. Very important!
<3> For unit testing the plugin.

This should leave your project with the following basic tree structure.

[literal]
....
.
├── build.gradle
├── gradle
│   └── wrapper
│       ├── gradle-wrapper.jar
│       └── gradle-wrapper.properties
├── gradlew
├── gradlew.bat
├── settings.gradle
└── src
    ├── main
    │   ├── groovy
    │   └── resources
    │       └── META-INF
    │           └── gradle-plugins
    └── test
        └── groovy
....

'''

*Next*: link:creating-the-skeleton-plugin.html[Creating the Skeleton Plugin]
